import firebase from 'firebase';

var config = {
  apiKey: 'AIzaSyDfOPiZ_4_rw4T0XOPAq3r53thzblaZblw',
  authDomain: 'cropchat-40042.firebaseapp.com',
  databaseURL: 'https://cropchat-40042.firebaseio.com',
  storageBucket: 'cropchat-40042.appspot.com',
  messagingSenderId: '113640716818'
};

firebase.initializeApp(config);

export default {
  database: firebase.database()
};